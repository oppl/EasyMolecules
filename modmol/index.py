index_html = """
<!DOCTYPE html>
<html style="height:100%">
<head>
	<meta charset="utf-8">
	<title>ModMol</title>
	
	<script type="text/javascript" src="jsmol/JSmol.min.js"></script>
<!-- 	//WebGL	
	<script type="text/javascript" src="jsmol/JSmol.GLmol.min.js"></script>
	<script type="text/javascript" src="jsmol/js/JSmolThree.js"></script>
 -->		
<!-- 	<script type="text/javascript" src="jsmol/jquery/jquery.js"></script> -->
	<script type="text/javascript" src="jsmol/js/JSmoljQueryExt.js"></script>
	<script type="text/javascript" src="jsmol/js/JSmolCore.js"></script>
	<script type="text/javascript" src="jsmol/js/JSmolApplet.js"></script>
	<script type="text/javascript" src="jsmol/js/JSmolApi.js"></script>
	<script type="text/javascript" src="jsmol/j2s/j2sjmol.js"></script>
	<script type="text/javascript" src="jsmol/js/JSmol.js"></script>
	<script type="text/javascript">

		var JSmolInfo = {
		height: '100%',
		width: '100%',
		j2sPath: "jsmol/j2s",
		use: 'HTML5',
		language: "fr",
		script: "set disablePopupMenu on; set frank off;",
		color: "black",
// 		console: "myJSmol_infodiv",
		disableJ2SLoadMonitor: true,
		disableInitialConsole: true,
		debug: false
		};
	  var myJSmol;
	  </script>
	
</head>
<body style="height:95%" bgcolor="black">

	    <script type="text/javascript">			
	    myJSmol = Jmol.getApplet("myJSmol", JSmolInfo);
    //         Jmol.script(monJSmol, 'LOAD MENU "test.mnu";' );
	    Jmol.script(myJSmol, 'load "molecules/simples/eau.pdb";' );
	    
	    </script>    

</body>
</html>
"""