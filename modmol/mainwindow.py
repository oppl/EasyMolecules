import sys
from PyQt4 import QtCore, QtGui, Qt
QtCore.Signal = QtCore.pyqtSignal
from .ui.mainwindow import Ui_MainWindow
from functools import partial
from .moleculesWidget import Accordion
from.molecules_db import DATA

class Main(QtGui.QMainWindow):
    
    def __init__(self):
        super().__init__()        
        #UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)        
        self.ui.actionAnaglyphe.setCheckable(True)
        self.ui.actionAnaglyphe.setIcon(QtGui.QIcon("./modmol/images/3dglasses.png"))
        self.ui.actionAntialiasing.setCheckable(True)
        self.ui.actionLiaisons_multiples.setCheckable(True)
        self.ui.actionLiaisons_multiples.setIcon(QtGui.QIcon("./modmol/images/double.png"))
        self.toolbox = Accordion(self,DATA)               
        self.ui.dockWidget.setWidget(self.toolbox)
        self.html = self.ui.html
        self.html.page().mainFrame().setScrollBarPolicy(QtCore.Qt.Vertical, QtCore.Qt.ScrollBarAlwaysOff)
        self.html.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)        
        self.default_url = "index.html"
        self.browse()
        self.setColorActions()
        self.setModelsActions()
        self.ui.toolbar.addAction(self.ui.actionLiaisons_multiples)
        self.actionTabPer = QtGui.QAction(QtGui.QIcon("./modmol/images/tabper.png"),"Tableau périodique", self)
        self.ui.toolbar.addAction(self.actionTabPer)
        
        #SLOTS
        self.actionTabPer.triggered.connect(self.show_tab_per)
        self.ui.actionQuitter.triggered.connect(self.quitter)
        self.ui.actionExporter.triggered.connect(self.exporter)
        self.toolbox.newMolecule.connect(self.load_mol) 
        self.ui.actionBlanc.triggered.connect(self.test_bg)
        self.ui.actionA_propos_de_Qt.activated.connect(self.slotAboutQt)
        self.ui.actionA_propos.activated.connect(self.slotAbout)
        self.ui.actionAnaglyphe.triggered.connect(self.set_anaglyphe)
        self.ui.actionAntialiasing.triggered.connect(self.set_antialiasing)
        self.ui.actionLiaisons_multiples.triggered.connect(self.set_multipleBonds)
        self.ui.actionOuvrir.triggered.connect(self.open_file)
    
    def open_file(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Ouvrir un fichier", QtCore.QDir.home().absolutePath(), "Molécules(*.pdb *.mol *.cml *.xyz);;Fichier PDB(*.pdb);;Fichier MOL(*.mol);;Fichier CML(*.cml);;Fichier XYZ(*.xyz)")
        if filename:
            frame = self.html.page().currentFrame()
            model = self.findChild(QtGui.QActionGroup, "models").checkedAction().text()
            if model == 'Modèle éclaté':
                mod = 'select *;cartoons off;spacefill 23%;wireframe 0.15'
            elif model == 'Modèle compact':
                mod = 'select *;cartoons off;spacefill only'
            script =  'load {0}; {1}'.format(filename,mod)
            frame.evaluateJavaScript("Jmol.script(myJSmol, '{0}')".format(script))     
    
    def show_tab_per(self):
        frame = self.html.page().currentFrame()
        mod = 'select *;cartoons off;spacefill only'
        molecule = 'molecules/tabper.pdb'
        script =  'load {0}; {1}'.format(molecule,mod)
        frame.evaluateJavaScript("Jmol.script(myJSmol, '{0}')".format(script))
    
    def set_anaglyphe(self):
        frame = self.html.page().currentFrame()
        if self.ui.actionAnaglyphe.isChecked():
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'stereo REDCYAN')")
        else :
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'stereo OFF')")
            
    def set_antialiasing(self):
        frame = self.html.page().currentFrame()
        if self.ui.actionAntialiasing.isChecked():
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'set antialiasDisplay ON')")
        else :
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'set antialiasDisplay OFF')")


    def set_multipleBonds(self):
        frame = self.html.page().currentFrame()
        if self.ui.actionLiaisons_multiples.isChecked():
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'set bonds ON')")
        else :
            frame.evaluateJavaScript("Jmol.script(myJSmol, 'set bonds OFF')")
                    
    
    def load_mol(self,mol):
        frame = self.html.page().currentFrame()
        model = self.findChild(QtGui.QActionGroup, "models").checkedAction().text()
        if model == 'Modèle éclaté':
            mod = 'select *;cartoons off;spacefill 23%;wireframe 0.15'
        elif model == 'Modèle compact':
            mod = 'select *;cartoons off;spacefill only'
        molecule = 'molecules/'+mol[1]
        script =  'load {0}; {1}'.format(molecule,mod)
        frame.evaluateJavaScript("Jmol.script(myJSmol, '{0}')".format(script))

    def exporter(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Exporter comme image", QtCore.QDir.home().absolutePath(), "Image PNG(*.png);;Image JPEG(*.jpg);;Image BMP(*.bmp)")
        if filename:
            frame = self.html.page().currentFrame()
            image = QtGui.QImage(self.html.page().viewportSize(), QtGui.QImage.Format_ARGB32)
            painter = QtGui.QPainter(image)
            frame.render(painter)
            painter.end()            
            image.save(filename, quality=90)

        
    def slotAboutQt(self):
        QtGui.QMessageBox.aboutQt(self)
        
    def slotAbout(self):
        message = """Modèles moléculaires v0.1
Licence : GPL v3
Python : 
PyQt : 
Webkit :
JSmol : http://wiki.jmol.org"""
        QtGui.QMessageBox.about(self,
                          "Modmol",
                          message)
        
    def test_bg(self):
        self.set_bg_color('white')
        
    def quitter(self) :
        sys.exit()   
        
    def change_model(self):
        frame = self.html.page().currentFrame()
        model = self.findChild(QtGui.QActionGroup, "models").checkedAction().text()
        if model == 'Modèle éclaté':
            frame.evaluateJavaScript("Jmol.script(myJSmol,'select *;cartoons off;spacefill 23%;wireframe 0.15')")
        elif model == 'Modèle compact':
            frame.evaluateJavaScript("Jmol.script(myJSmol,'select *;cartoons off;spacefill only')")

        
    
    def setModelsActions(self):        
        models = (('Modèle éclaté','eclate.png'),
            ('Modèle compact','compact.png')) 
        self.modelsActionGroup = QtGui.QActionGroup(self, exclusive=True, objectName="models", triggered=self.change_model)        
        for model in models :
            action = QtGui.QAction(QtGui.QIcon("./modmol/images/"+model[1]),model[0], self)
            action.setCheckable(True)
            self.modelsActionGroup.addAction(action)
            self.ui.toolbar.addAction(action)
            self.ui.menuMod_le.addAction(action)
        self.modelsActionGroup.actions()[0].setChecked(True)

        
    
    def setColorActions(self):        
        colors = (('Noir','black'),
                  ('Gris', 'gray'),
                  ('Blanc','white'))
        self.colorsActionGroup = QtGui.QActionGroup(self, exclusive=True, objectName="colors")#, triggered = partial(self.set_bg_color, color[1]))
        for color in colors:
            action = QtGui.QAction(color[0], self)
            action.setCheckable(True)
            self.colorsActionGroup.addAction(action)
            self.ui.menuArri_re_plan.addAction(action)
            QtCore.QObject.connect(action, QtCore.SIGNAL("triggered()"), partial(self.set_bg_color, color[1]))
        self.colorsActionGroup.actions()[0].setChecked(True)
        
    def set_bg_color(self,color):
        try :
            color_html = QtGui.QColor(color).name()
        except:
            color_html = 'black'
        frame = self.html.page().currentFrame()
        frame.evaluateJavaScript("Jmol.script(myJSmol,'background  [x{0}]')".format(color_html[1:]))
        frame.evaluateJavaScript("document.body.style.backgroundColor = '{0}';".format(color_html))

    def browse(self):
        """
            Make a web browse on a specific url and show the page on the
            Webview widget.
        """

        #url = self.tb_url.text() if self.tb_url.text() else self.default_url
        url = self.default_url
        self.html.load(QtCore.QUrl(url))

        #self.html.load(url)
        self.html.show()
        #self.set_bg_color('red')