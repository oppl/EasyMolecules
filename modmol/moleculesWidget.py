from PyQt4 import QtCore, QtGui

class Category(QtGui.QListWidget):

    def __init__(self, parent, molecules=None):
        super(Category, self).__init__()
        #mapper = QtCore.QSignalMapper(self)
        #moleculeChanged = QtCore.pyqtSignal(int)
        #self.moleculeChanged.connect(self.emit_molecule)
        #print(molecules)
        
        
        if molecules :
            self.molecules = molecules
        else :
            self.molecules = (('eau','eau.pdb'),('méthane','methane.pdb'))

        for molecule in molecules :
            self.addItem(molecule[0])
            
    def get_molecule(self):
        currentItem = self.currentRow()
        return self.molecules[currentItem]
    
    def clear_selection(self):
        for i in range(self.count()):
            item = self.item(i)
            self.setItemSelected(item, False)

class Accordion(QtGui.QToolBox):
    
    newMolecule = QtCore.pyqtSignal(tuple)
    
    def __init__(self, parent, data=None):
        super(Accordion, self).__init__()
        
        if data :
            pass
        else : 
            data = (('Simples',(('eau','eau.pdb'),('méthane','methane.pdb'))),('Alcanes', (('méthane','methane.pdb'),('éthane','ethane.pdb'))))
        for cat in data :
            item = Category(self, cat[1])
            self.addItem(item,cat[0])
            item.itemSelectionChanged.connect(self.new_molecule_selected)
            item.clicked.connect(self.send_new_molecule)
            
    def new_molecule_selected(self):
        #mol = self.currentWidget().get_molecule()
        categories_to_clear = list(range(self.count()))
        del categories_to_clear[self.currentIndex()]
        #print(items_to_clear)
        for cat in categories_to_clear:
            self.widget(cat).clear_selection()
            
    def send_new_molecule(self):
        self.newMolecule.emit(self.currentWidget().get_molecule())
