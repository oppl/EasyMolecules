DATA = (
            ("Simples",
                (
                ("eau","eau.pdb"),
                ("dihydrogène","dihydrogene.mol"),
                ("chlorure d'hydrogène","hcl.mol"),
                ("ammoniac","ammoniac.pdb"),
                ("dioxygène","dioxygene.mol"),
                ("diazote","n2.mol"),
                ("dioxyde de carbone","co2.mol"),
                ("méthane","methane.pdb"),
                )
            ),
            ("Alcanes", 
                (
                ("méthane","methane.pdb"),
                ("éthane","ethane.pdb")
                )
            ),
            ("Médicaments", 
                (
                ("aspirine","aspirine.mol"),
                ("paracétamol","paracetamol.mol"),
                ("ibuprofène","ibuprofene.cml"),
                ("vitamine C","vitamine-C.mol")
                )
            )

        )