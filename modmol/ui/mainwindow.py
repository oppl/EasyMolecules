# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'modmol/ui/mainwindow.ui'
#
# Created: Fri Mar 20 16:30:37 2015
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(882, 595)
        MainWindow.setIconSize(QtCore.QSize(24, 24))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.html = QtWebKit.QWebView(self.centralwidget)
        self.html.setObjectName(_fromUtf8("html"))
        self.gridLayout.addWidget(self.html, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 882, 20))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFichier = QtGui.QMenu(self.menubar)
        self.menuFichier.setObjectName(_fromUtf8("menuFichier"))
        self.menuAffichage = QtGui.QMenu(self.menubar)
        self.menuAffichage.setObjectName(_fromUtf8("menuAffichage"))
        self.menuMod_le = QtGui.QMenu(self.menuAffichage)
        self.menuMod_le.setObjectName(_fromUtf8("menuMod_le"))
        self.menuArri_re_plan = QtGui.QMenu(self.menuAffichage)
        self.menuArri_re_plan.setObjectName(_fromUtf8("menuArri_re_plan"))
        self.menuAide = QtGui.QMenu(self.menubar)
        self.menuAide.setObjectName(_fromUtf8("menuAide"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.toolbar = QtGui.QToolBar(MainWindow)
        self.toolbar.setMovable(True)
        self.toolbar.setAllowedAreas(QtCore.Qt.AllToolBarAreas)
        self.toolbar.setIconSize(QtCore.QSize(32, 32))
        self.toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.toolbar.setObjectName(_fromUtf8("toolbar"))
        MainWindow.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolbar)
        self.dockWidget = QtGui.QDockWidget(MainWindow)
        self.dockWidget.setMinimumSize(QtCore.QSize(200, 77))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.dockWidget.setFont(font)
        self.dockWidget.setFeatures(QtGui.QDockWidget.AllDockWidgetFeatures)
        self.dockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        self.dockWidget.setObjectName(_fromUtf8("dockWidget"))
        self.dockWidgetContents = QtGui.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.dockWidget.setWidget(self.dockWidgetContents)
        MainWindow.addDockWidget(QtCore.Qt.DockWidgetArea(2), self.dockWidget)
        self.actionOuvrir = QtGui.QAction(MainWindow)
        self.actionOuvrir.setObjectName(_fromUtf8("actionOuvrir"))
        self.actionQuitter = QtGui.QAction(MainWindow)
        self.actionQuitter.setObjectName(_fromUtf8("actionQuitter"))
        self.actionBlanc = QtGui.QAction(MainWindow)
        self.actionBlanc.setObjectName(_fromUtf8("actionBlanc"))
        self.actionGris_clair = QtGui.QAction(MainWindow)
        self.actionGris_clair.setObjectName(_fromUtf8("actionGris_clair"))
        self.actionGris_fonc = QtGui.QAction(MainWindow)
        self.actionGris_fonc.setObjectName(_fromUtf8("actionGris_fonc"))
        self.actionNoir = QtGui.QAction(MainWindow)
        self.actionNoir.setObjectName(_fromUtf8("actionNoir"))
        self.actionAnaglyphe = QtGui.QAction(MainWindow)
        self.actionAnaglyphe.setObjectName(_fromUtf8("actionAnaglyphe"))
        self.actionExporter = QtGui.QAction(MainWindow)
        self.actionExporter.setObjectName(_fromUtf8("actionExporter"))
        self.actionEclat = QtGui.QAction(MainWindow)
        self.actionEclat.setObjectName(_fromUtf8("actionEclat"))
        self.actionCompact = QtGui.QAction(MainWindow)
        self.actionCompact.setObjectName(_fromUtf8("actionCompact"))
        self.actionA_propos = QtGui.QAction(MainWindow)
        self.actionA_propos.setObjectName(_fromUtf8("actionA_propos"))
        self.actionBarre_d_outils = QtGui.QAction(MainWindow)
        self.actionBarre_d_outils.setObjectName(_fromUtf8("actionBarre_d_outils"))
        self.actionA_propos_de_Qt = QtGui.QAction(MainWindow)
        self.actionA_propos_de_Qt.setObjectName(_fromUtf8("actionA_propos_de_Qt"))
        self.actionAntialiasing = QtGui.QAction(MainWindow)
        self.actionAntialiasing.setObjectName(_fromUtf8("actionAntialiasing"))
        self.actionLiaisons_multiples = QtGui.QAction(MainWindow)
        self.actionLiaisons_multiples.setObjectName(_fromUtf8("actionLiaisons_multiples"))
        self.menuFichier.addAction(self.actionOuvrir)
        self.menuFichier.addAction(self.actionExporter)
        self.menuFichier.addSeparator()
        self.menuFichier.addAction(self.actionQuitter)
        self.menuAffichage.addSeparator()
        self.menuAffichage.addAction(self.menuMod_le.menuAction())
        self.menuAffichage.addAction(self.menuArri_re_plan.menuAction())
        self.menuAffichage.addAction(self.actionLiaisons_multiples)
        self.menuAffichage.addSeparator()
        self.menuAffichage.addAction(self.actionAnaglyphe)
        self.menuAffichage.addAction(self.actionAntialiasing)
        self.menuAide.addAction(self.actionA_propos)
        self.menuAide.addAction(self.actionA_propos_de_Qt)
        self.menubar.addAction(self.menuFichier.menuAction())
        self.menubar.addAction(self.menuAffichage.menuAction())
        self.menubar.addAction(self.menuAide.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Modèles moléculaires", None))
        self.menuFichier.setTitle(_translate("MainWindow", "Fichier", None))
        self.menuAffichage.setTitle(_translate("MainWindow", "Affichage", None))
        self.menuMod_le.setTitle(_translate("MainWindow", "Modèle", None))
        self.menuArri_re_plan.setTitle(_translate("MainWindow", "Arrière-plan", None))
        self.menuAide.setTitle(_translate("MainWindow", "Aide", None))
        self.toolbar.setWindowTitle(_translate("MainWindow", "Barre d\'outils", None))
        self.dockWidget.setWindowTitle(_translate("MainWindow", "Molécules", None))
        self.actionOuvrir.setText(_translate("MainWindow", "Ouvrir", None))
        self.actionQuitter.setText(_translate("MainWindow", "Quitter", None))
        self.actionBlanc.setText(_translate("MainWindow", "Blanc", None))
        self.actionGris_clair.setText(_translate("MainWindow", "Gris clair", None))
        self.actionGris_fonc.setText(_translate("MainWindow", "Gris foncé", None))
        self.actionNoir.setText(_translate("MainWindow", "Noir", None))
        self.actionAnaglyphe.setText(_translate("MainWindow", "Anaglyphe", None))
        self.actionExporter.setText(_translate("MainWindow", "Exporter", None))
        self.actionEclat.setText(_translate("MainWindow", "Eclaté", None))
        self.actionCompact.setText(_translate("MainWindow", "Compact", None))
        self.actionA_propos.setText(_translate("MainWindow", "A propos", None))
        self.actionBarre_d_outils.setText(_translate("MainWindow", "Barre d\'outils", None))
        self.actionA_propos_de_Qt.setText(_translate("MainWindow", "A propos de Qt", None))
        self.actionAntialiasing.setText(_translate("MainWindow", "anticrénelage", None))
        self.actionLiaisons_multiples.setText(_translate("MainWindow", "Liaisons multiples", None))

from PyQt4 import QtWebKit
from . import resources_rc
